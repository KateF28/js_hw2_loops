let userNumber = Number(prompt('Enter a positive integer', '18'));
document.write(`Simple number is 2<br \/>`);
nextStep:
for (let simpleNumber = 3; simpleNumber <= userNumber; simpleNumber++) {
  for (let divideNumber = 2; divideNumber <= (Math.sqrt(simpleNumber)).toFixed(); divideNumber++) {
		if (simpleNumber % divideNumber === 0) continue nextStep;
	};
		document.write(`Simple number is ${simpleNumber} <br \/>`);
};

// nextPrime:
//   for (var i = 2; i <= 10; i++) {
//     for (var j = 2; j < i; j++) {
//       if (i % j == 0) continue nextPrime;
//     }
//     alert( i );
//   }

// число x является простым, если оно больше 1 
// и при этом делится без остатка только на 1 и на x. 
// 5 — простое число, а 6 является составным, так как, помимо 1 и 6, также делится на 2 и 3